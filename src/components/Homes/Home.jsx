import React, { useState } from "react";
import axios from "axios";
import { useEffect } from "react";
import { BiPlay } from "react-icons/bi";
import { AiOutlinePlus } from "react-icons/ai";
import "./Home.scss";

const apikey = "79f654d36ee3eab8c4b6d27e884e421a";
const url = "https://api.themoviedb.org/3/movie/";
const upcoming = "upcoming";
const imgurl = "https://image.tmdb.org/t/p/original/";
const popular = "popular";
const top_rated = "top_rated";
const Now_playing = "now_playing";

const Card = ({ img }) => <img className="card" src={img} alt="cover" />;

const Row = ({
  title,
  arr = [
    {
      img: "https://deadline.com/wp-content/uploads/2022/09/THE-FAMILY_GALLERY_0072RC5.jpg?w=1024",
    },
  ],
}) => (

  <div className="row">

    <h2>{title}</h2>

    <div>

      {arr.map((item, index) => (
       
        <Card key={index} img={`${imgurl}/${item.poster_path}`} />
      ))}

    </div>

  </div>
);

const Home = () => {
  const [upcomingMoives, setUcomingMoives] = useState([]);
  const [popularMoives, setPopularMoives] = useState([]);
  const [top_ratedMoives, setTop_rated] = useState([]);
  const [now_playing, setNow_playingMoives] = useState([]);

  useEffect(() => {

    const fetchupcoming = async () => {

      const {

        data: { results },

      } = await axios.get(`${url}/${upcoming}?api_key=${apikey}`);

      setUcomingMoives(results);
    };

    const fetchpopular = async () => {

      const {

        data: { results },

      } = await axios.get(`${url}/${popular}?api_key=${apikey}`);

      setPopularMoives(results);

    };

    const fetchtop_rated = async () => {

      const {

        data: { results },

      } = await axios.get(`${url}/${top_rated}?api_key=${apikey}`);
      
      setTop_rated(results);

    };

    const fetchlatest = async () => {

      const {

        data: { results },

      } = await axios.get(`${url}/${Now_playing}?api_key=${apikey}`);

      setNow_playingMoives(results);

    };

    fetchupcoming();
    fetchpopular();
    fetchlatest();
    fetchtop_rated();

  }, []);

  return (
    <section className="home">

      <div
        className="banner"
        style={{
          backgroundImage: popularMoives[1]
            ? `url(${`${imgurl}/${popularMoives[0].poster_path}`})`
            : " rgb(16, 16, 16)",
        }}
      >

        <div>

        {popularMoives[0] && <h1>{popularMoives[0].original_title}</h1>}

        {popularMoives[0] && <p>{popularMoives[0].overview}</p>}

        </div>

        <div>

          <button>

            <BiPlay style={{ color: "black" }} /> Play

          </button>
          
          <button>

            My List <AiOutlinePlus style={{ color: "black" }} />

          </button>

        </div>

      </div>

      <Row title={"Upcoming Movies"} arr={upcomingMoives} />

      <Row title={"Popular Movies"} arr={popularMoives} />

      <Row title={"Top_rated Movies"} arr={top_ratedMoives} />

      <Row title={"Latest Movies"} arr={now_playing} />

    </section>

  );
  
};

export default Home;
